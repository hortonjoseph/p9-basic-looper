//
//  Looper.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "Looper.h"

Looper::Looper() :  recordState (false),    //initialise - not playing
playState (false),      //not recording
bufferPosition (0)      //position to the start of audioSampleBuffer
{
    
    playState = false;
    recordState = false;
    bufferPosition = 0;
    
    audioSampleBuffer.setSize (1, bufferSize);
    audioSampleBuffer.clear();
    
    //audioSampleBuffer contents to zero
    //    for (int count = 0; count < bufferSize; count++)
    //        audioSampleBuffer[count] = 0.f;
}

Looper::~Looper()
{
    
}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState.get();
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState.get();
}

void Looper::setSaveState(const bool newState)
{
    saveState = newState;
}

bool Looper::getSaveState() const
{
    return saveState.get();
}

float Looper::processSample (float input)
{
    float output = 0.f;
    float *audioSample;
    
    if (playState.get() == true)
    {
        //play
        output = *audioSampleBuffer.getWritePointer(0, bufferPosition);
        
        //click 4 times each bufferLength
        if ((bufferPosition % (bufferSize / 8)) == 0)
            output += 0.25f;
        
        //record
        if (recordState.get() == true)
        {
            audioSample = audioSampleBuffer.getWritePointer(0, bufferPosition);
            *audioSample += (input*0.75);
        }
        
        //increment and cycle the buffer counter
        ++bufferPosition;
        if (bufferPosition == bufferSize)
            bufferPosition = 0;
    }
    return output;
}

void Looper::save()
{
    FileChooser chooser ("Please select a file...", File::getSpecialLocation(File::userDesktopDirectory), "*.wav");
    if (playState.get() == false)
    {
        if (chooser.browseForFileToSave(true))
        {
            File file (chooser.getResult().withFileExtension("*.wav"));
            OutputStream* outStream = file.createOutputStream();
            WavAudioFormat wavFormat;
            AudioFormatWriter* writer = wavFormat.createWriterFor (outStream, 44100, 1, 16, NULL, 0);
            writer->writeFromAudioSampleBuffer(audioSampleBuffer, 0, audioSampleBuffer.getNumSamples());
            delete writer;
        }
    }
    
}
