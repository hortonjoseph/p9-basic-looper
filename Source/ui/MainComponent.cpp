/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent (Audio& audio_)
 :  audio (audio_),
    looper1Gui (audio_.getLooper1()), looper2Gui(audio_.getLooper2())
{
    setSize (500, 400);
    addAndMakeVisible (looper1Gui);
    addAndMakeVisible (looper2Gui);
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    looper1Gui.setSize (getWidth(), 40);
    looper2Gui.setSize (getWidth(), 40);
    looper2Gui.setTopLeftPosition(0, looper1Gui.getBottom()+2);
    
}

//MenuBarCallbacks==============================================================
StringArray MainComponent::getMenuBarNames()
{
    const char* const names[] = { "File", 0 };
    return StringArray (names);
}

PopupMenu MainComponent::getMenuForIndex (int topLevelMenuIndex, const String& menuName)
{
    PopupMenu menu;
    if (topLevelMenuIndex == 0)
        menu.addItem(AudioPrefs, "Audio Prefrences", true, false);
    return menu;
}

void MainComponent::menuItemSelected (int menuItemID, int topLevelMenuIndex)
{
    if (topLevelMenuIndex == FileMenu)
    {
        if (menuItemID == AudioPrefs)
        {
            AudioDeviceSelectorComponent audioSettingsComp (audio.getAudioDeviceManager(),
                                                            0, 2, 2, 2, true, true, true, false);
            audioSettingsComp.setSize (450, 350);
            DialogWindow::showModalDialog ("Audio Settings",
                                           &audioSettingsComp, this, Colours::lightgrey, true);
        }
    }
}

